var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var formidable =require('formidable');
const XLSX = require('xlsx');

const authenticator=require('./authenticator.js')
const userDetailsApi=require('./api/userDetailsAPI');
const decodeIDToken = require('./api/verifyToken');
const database=require('./api/databaseUtil');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, '../react-frontend/build')));
app.use(decodeIDToken);

app.get("/", function (req, res) {
  res.sendFile(path.resolve(__dirname, "../react-frontend/build", 'index.html'));
})
app.use('/', indexRouter);
app.use('/file-upload', function(req,res){

  const form = new formidable.IncomingForm();
  form.uploadDir = "./";
  form.keepExtensions = true;
  form.parse(req, async(err, fields, files) => {
   let result= readExcelFiles(files);
   if(fields.methodName=='saveOperatorEntry'){
    result=database.saveOPeratorEntry(result,fields.userId);
   }else{
  result=await database.saveMachineParts(result,fields.userId);
   }
    res.status(200).json({ name:  result })
  });

});
readExcelFiles=(files)=>{
let sheetJson;
    let file=files.file;
    const workbook=XLSX.readFile(file.path);
    const sheet_name_list=workbook.SheetNames;
    sheetJson=XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
  return sheetJson;

}
app.use('/authenticate',async (req,res)=>{
  let userName=req.body.userName;
  let password=req.body.password;
  let status='';
  try{
  status=await authenticator.authenticate({'userName':userName,'password':password});
  let uid=status.data.user.uid;

  }catch(err){
    status=err;
  }
  res.status(200).json(status)
});
app.use('/getUserDetails',async (req,res)=>{

  const auth = req.currentUser;
  if(auth){
    let userId=req.body.uid;
    console.log('user id : ',userId);
    res.status(200).json({'status': await userDetailsApi.getUserDetails(userId)});
  }
  return res.status(403).send('Not authorized');

})
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;
