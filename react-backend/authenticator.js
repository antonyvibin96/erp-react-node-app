const firebaseClient = require ("./firebaseClient");
const firebase =require("firebase/app");
require('firebase/auth');
let methods = {
	 authenticate: async function(data) {
        firebaseClient.firebaseClient();
        try{
         let firebaseUser= await firebase
              .auth()
              .signInWithEmailAndPassword(data.userName, data.password);
              return {'status':true,'data':firebaseUser};

        }catch(err){
          return {'status':false,'data':err};
        }          
	},
	currentDate: function() {
		console.log('Current Date is: ' + new Date().toISOString().slice(0, 10));
	}
};
 
module.exports = methods;