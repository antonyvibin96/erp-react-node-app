const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");


  
const firebaseConfig = {
    apiKey: "AIzaSyCNPfKitdYxmT0BmIEJJz697-DYi-yZSZg",
    authDomain: "erp-nextjs.firebaseapp.com",
    projectId: "erp-nextjs",
    storageBucket: "erp-nextjs.appspot.com",
    messagingSenderId: "1067540823721",
    appId: "1:1067540823721:web:f94a9a98b38222f6d233b8",
    measurementId: "G-ET7HJBSQPY"
};

try {
  firebase.initializeApp(firebaseConfig);
} catch (err) {
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack);
  }
}

let db = firebase.firestore();
let database=async ()=> {

    try {
        firebase.initializeApp(firebaseConfig);
      } catch (err) {
        if (!/already exists/.test(err.message)) {
          console.error('Firebase initialization error', err.stack);
        }
      }

    if(!db){
        db=firebase.firestore();
    }
    return db;
}
let methods={
getUserDetails:async (userId)=>{

    let db=await database();


        var docRef = db.collection(userId).doc("USER-DETAILS");
        

        try{
let doc = await docRef.get(); 
    if (doc.exists) {
        console.log("Document data:", doc.data());
        return doc.data();
    } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
        return 'No such document!';
    }
        }catch(err){
            console.log("No such document!");
            return 'No such document!'+err;
        }

},
saveMachineParts:async(data,userId)=>{

    let db=await database();
    var docRef = db.collection(userId).doc("MACHINE-PARTS");
    let status='';
    try{

        await docRef.set({
            partName:data
        });
        status='success';
    }catch(err){
        console.log('error : ',err);
        status='error';
    }

    return status;
},
saveOPeratorEntry:(data,userId)=>{
    return data;
}
}

 



  module.exports=methods