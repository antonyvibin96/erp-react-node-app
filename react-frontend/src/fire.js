import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyCNPfKitdYxmT0BmIEJJz697-DYi-yZSZg",
    authDomain: "erp-nextjs.firebaseapp.com",
    projectId: "erp-nextjs",
    storageBucket: "erp-nextjs.appspot.com",
    messagingSenderId: "1067540823721",
    appId: "1:1067540823721:web:f94a9a98b38222f6d233b8",
    measurementId: "G-ET7HJBSQPY"
};

try {
  firebase.initializeApp(firebaseConfig);
} catch (err) {
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack);
  }
}

const fire = firebase;
export default fire;