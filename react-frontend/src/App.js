import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import LandingPage from './landing-page';
import FileUpload from './file-upload';
import {page404} from './404-page';
import Login from './components/login';
import { useState,useEffect } from 'react';

import fire from './fire.js';
import {post,createToken} from './api/ExpressAPI.js' 
import axios from 'axios';

function App() {

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isFileUploaded, setIsFileUploaded] = useState(false);
  
    fire.auth().onAuthStateChanged(async (user) => {
      return user ? setIsLoggedIn(true) : setIsLoggedIn(false);
      
  });

  useEffect( async ()=>{

    if(isLoggedIn){
      try{
        let user=fire.auth().currentUser;
        // let users=await axios.post("/getUserDetails",user.uid,header);
          let userDetails=await post('/getUserDetails',{uid:user.uid});
          console.log("user Details : "+JSON.stringify(userDetails));
          if(userDetails.status.ROLE!='ADMIN'){
            signOut();
          }
          setIsFileUploaded(userDetails.status.IS_FILE_UPLOADED);
       }catch(err){
          console.log(err);
       }
    }

  },[isLoggedIn]);


  
  
  const signOut = () => {
    fire.auth().signOut()
  };
  


    return(
      <div className="App">
         <Router>
        {!isLoggedIn
          ? (
            <>
            <Switch>
              <Route path="/">
                <Login />
              </Route>
            </Switch>
            </>
          ) 
          : (
            
            <>
            <span onClick={signOut}>
              <a href="#">Sign out</a>
            </span>
            <Switch>
            <Route exact path='/' render={(routeProps) => (
  <LandingPage isFileUploaded={isFileUploaded} />
)} />
  <Route path='/file-upload' component={FileUpload}/>
  <Route path='*' component={()=>'404 not found!!'} />
            </Switch>
            </>
          
          
          )}
      </Router>
</div>
    )
  
}

export default App;