import axios from 'axios';
import fire from '../fire';

const url = 'http://localhost:3001/api';

export const createToken = async () => {
  const user = fire.auth().currentUser;
  const token = user && (await user.getIdToken());
  console.log(token);

  const payloadHeader = {
    headers: {
    //  'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  };
  return payloadHeader;
}

export const post = async (url, payload)=>{

    const header = await createToken();
    try{
        const res = await axios.post(url, payload, header);
      return res.data;
    } catch (e) {
        console.error(e);
      }

}