import {Link} from 'react-router-dom';


const getLandingPage=(props)=>{
  console.log(props.isFileUploaded);
  if(!props.isFileUploaded){
   return <Link to="/file-upload">File Upload</Link>
    }
}

const LandingPage = (props) => {
    return (
      <>
        <div>Landing page</div>
        {getLandingPage(props)}
        </>
      );
}

 
export default LandingPage;